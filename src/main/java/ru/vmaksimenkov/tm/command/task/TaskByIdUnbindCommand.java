package ru.vmaksimenkov.tm.command.task;

import ru.vmaksimenkov.tm.exception.entity.TaskNotFoundException;
import ru.vmaksimenkov.tm.util.TerminalUtil;

public final class TaskByIdUnbindCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Unbind task by id";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[UNBIND TASK BY ID]");
        if (serviceLocator.getTaskService().size(userId) < 1) throw new TaskNotFoundException();
        System.out.println("ENTER TASK ID:");
        serviceLocator.getProjectTaskService().unbindTaskFromProject(userId, TerminalUtil.nextLine());
    }

    @Override
    public String name() {
        return "task-unbind-by-id";
    }

}
