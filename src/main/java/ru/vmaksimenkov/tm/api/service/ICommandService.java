package ru.vmaksimenkov.tm.api.service;

import ru.vmaksimenkov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandService {

    void add(AbstractCommand command);

    Collection<AbstractCommand> getArguments();

    AbstractCommand getCommandByArg(String arg);

    AbstractCommand getCommandByName(String name);

    Collection<AbstractCommand> getCommands();

    Collection<String> getListArgumentName();

    Collection<String> getListCommandName();

}
