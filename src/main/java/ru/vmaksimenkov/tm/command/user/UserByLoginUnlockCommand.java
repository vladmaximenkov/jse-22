package ru.vmaksimenkov.tm.command.user;

import ru.vmaksimenkov.tm.enumerated.Role;

import static ru.vmaksimenkov.tm.util.TerminalUtil.nextLine;

public final class UserByLoginUnlockCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Unlock user by login";
    }

    @Override
    public void execute() {
        System.out.println("[UNLOCK USER]");
        System.out.println("ENTER LOGIN:");
        serviceLocator.getUserService().unlockUserByLogin(nextLine());
    }

    @Override
    public String name() {
        return "user-unlock-by-login";
    }

    @Override
    public Role[] roles() { return new Role[] {Role.ADMIN }; }

}
