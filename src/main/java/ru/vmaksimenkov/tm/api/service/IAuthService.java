package ru.vmaksimenkov.tm.api.service;

import ru.vmaksimenkov.tm.enumerated.Role;
import ru.vmaksimenkov.tm.model.User;

public interface IAuthService {

    User getUser();

    String getUserId();

    boolean isAuth();

    void checkRoles(Role... roles);

    void login(String login, String password);

    void logout();

    void registry(String login, String password, String email);

}
