package ru.vmaksimenkov.tm.command.user;

import ru.vmaksimenkov.tm.model.User;

public final class UserViewProfileCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show user profile";
    }

    @Override
    public void execute() {
        final User user = serviceLocator.getAuthService().getUser();
        showUser(user);
    }

    @Override
    public String name() {
        return "user-view";
    }

}
