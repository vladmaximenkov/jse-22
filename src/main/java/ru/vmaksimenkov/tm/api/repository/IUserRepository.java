package ru.vmaksimenkov.tm.api.repository;

import ru.vmaksimenkov.tm.api.IRepository;
import ru.vmaksimenkov.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    boolean existsByEmail(String email);

    boolean existsByLogin(String login);

    boolean existsById(String id);

    User findById(String id);

    User findByLogin(String login);

    void removeByLogin(String login);

    void setPasswordById(String id, String password);

}
