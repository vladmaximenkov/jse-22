package ru.vmaksimenkov.tm.service;

import ru.vmaksimenkov.tm.api.repository.ITaskRepository;
import ru.vmaksimenkov.tm.api.service.ITaskService;
import ru.vmaksimenkov.tm.enumerated.Status;
import ru.vmaksimenkov.tm.exception.empty.EmptyIdException;
import ru.vmaksimenkov.tm.exception.empty.EmptyNameException;
import ru.vmaksimenkov.tm.exception.entity.TaskNotFoundException;
import ru.vmaksimenkov.tm.exception.system.IndexIncorrectException;
import ru.vmaksimenkov.tm.model.Task;

import static ru.vmaksimenkov.tm.util.ValidationUtil.checkIndex;
import static ru.vmaksimenkov.tm.util.ValidationUtil.isEmpty;

public class TaskService extends AbstractService<Task> implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Override
    public Task add(final String userId, final String name, final String description) {
        if (isEmpty(name)) throw new EmptyNameException();
        final Task task = new Task();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
        return task;
    }

    @Override
    public boolean existsByName(final String userId, final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        return taskRepository.existsByName(userId, name);
    }

    @Override
    public Task findOneByIndex(final String userId, final Integer index) {
        if (!checkIndex(index, taskRepository.size(userId))) throw new IndexIncorrectException();
        return taskRepository.findOneByIndex(userId, index);
    }

    @Override
    public Task findOneByName(final String userId, final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        return taskRepository.findOneByName(userId, name);
    }

    @Override
    public Task finishTaskById(final String userId, final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        final Task task = findById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public Task finishTaskByIndex(final String userId, final Integer index) {
        if (!checkIndex(index, taskRepository.size(userId))) throw new IndexIncorrectException();
        final Task task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public Task finishTaskByName(final String userId, final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        final Task task = findOneByName(userId, name);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public String getIdByIndex(final String userId, final Integer index) {
        if (!checkIndex(index, taskRepository.size(userId))) throw new IndexIncorrectException();
        return taskRepository.getIdByIndex(userId, index);
    }

    @Override
    public void removeOneById(final String userId, final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        taskRepository.removeById(userId, id);
    }

    @Override
    public void removeOneByIndex(final String userId, final Integer index) {
        if (!checkIndex(index, taskRepository.size(userId))) throw new IndexIncorrectException();
        taskRepository.removeOneByIndex(userId, index);
    }

    @Override
    public void removeOneByName(final String userId, final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        taskRepository.removeOneByName(userId, name);
    }

    @Override
    public Task setTaskStatusById(final String userId, final String id, final Status status) {
        if (isEmpty(id)) throw new EmptyIdException();
        final Task task = findById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task setTaskStatusByIndex(final String userId, final Integer index, final Status status) {
        if (!checkIndex(index, taskRepository.size(userId))) throw new IndexIncorrectException();
        final Task task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task setTaskStatusByName(final String userId, final String name, final Status status) {
        if (isEmpty(name)) throw new EmptyNameException();
        final Task task = findOneByName(userId, name);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task startTaskById(final String userId, final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        final Task task = findById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startTaskByIndex(final String userId, final Integer index) {
        if (!checkIndex(index, taskRepository.size(userId))) throw new IndexIncorrectException();
        final Task task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startTaskByName(final String userId, final String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        final Task task = findOneByName(userId, name);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task updateTaskById(final String userId, final String id, final String name, final String description) {
        if (isEmpty(id)) throw new EmptyIdException();
        if (isEmpty(name)) throw new EmptyNameException();
        final Task task = findById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateTaskByIndex(final String userId, final Integer index, final String name, final String description) {
        if (!checkIndex(index, taskRepository.size(userId))) throw new IndexIncorrectException();
        if (isEmpty(name)) throw new EmptyNameException();
        final Task task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateTaskByName(final String userId, final String name, final String nameNew, final String description) {
        if (isEmpty(name) || isEmpty(nameNew)) throw new EmptyNameException();
        final Task task = findOneByName(userId, name);
        if (task == null) throw new TaskNotFoundException();
        task.setName(nameNew);
        task.setDescription(description);
        return task;
    }

}
