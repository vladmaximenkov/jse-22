package ru.vmaksimenkov.tm.command;

import ru.vmaksimenkov.tm.api.service.ServiceLocator;
import ru.vmaksimenkov.tm.enumerated.Role;

public abstract class AbstractCommand {

    protected ServiceLocator serviceLocator;

    public AbstractCommand() {
    }

    public Role[] roles() { return null; }

    public abstract String arg();

    public abstract String description();

    public abstract void execute();

    public abstract String name();

    public void setServiceLocator(final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

}