package ru.vmaksimenkov.tm.api.repository;

import ru.vmaksimenkov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    void add(AbstractCommand command);

    Collection<AbstractCommand> getArguments();

    Collection<String> getCommandArgs();

    AbstractCommand getCommandByArg(String name);

    AbstractCommand getCommandByName(String name);

    Collection<String> getCommandNames();

    Collection<AbstractCommand> getCommands();

}