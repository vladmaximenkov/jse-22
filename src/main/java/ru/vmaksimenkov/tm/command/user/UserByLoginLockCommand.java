package ru.vmaksimenkov.tm.command.user;

import ru.vmaksimenkov.tm.enumerated.Role;
import ru.vmaksimenkov.tm.exception.entity.UserNotFoundException;
import ru.vmaksimenkov.tm.exception.user.AlreadyLoggedInException;
import ru.vmaksimenkov.tm.util.TerminalUtil;

import static ru.vmaksimenkov.tm.util.TerminalUtil.nextLine;

public final class UserByLoginLockCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Lock user by login";
    }

    @Override
    public void execute() {
        System.out.println("[LOCK USER]");
        System.out.println("ENTER LOGIN:");
        serviceLocator.getUserService().lockUserByLogin(nextLine());
    }

    @Override
    public String name() {
        return "user-lock-by-login";
    }

    @Override
    public Role[] roles() { return new Role[] {Role.ADMIN }; }

}
