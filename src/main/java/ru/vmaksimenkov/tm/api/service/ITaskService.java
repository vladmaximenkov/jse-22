package ru.vmaksimenkov.tm.api.service;

import ru.vmaksimenkov.tm.api.IService;
import ru.vmaksimenkov.tm.enumerated.Status;
import ru.vmaksimenkov.tm.model.Task;

public interface ITaskService extends IService<Task> {

    Task add(String userId, String name, String description);

    boolean existsByName(String userId, String name);

    Task findOneByIndex(String userId, Integer index);

    Task findOneByName(String userId, String name);

    Task finishTaskById(String userId, String id);

    Task finishTaskByIndex(String userId, Integer index);

    Task finishTaskByName(String userId, String name);

    String getIdByIndex(String userId, Integer index);

    void removeOneById(String userId, String id);

    void removeOneByIndex(String userId, Integer index);

    void removeOneByName(String userId, String name);

    Task setTaskStatusById(String userId, String id, Status status);

    Task setTaskStatusByIndex(String userId, Integer index, Status status);

    Task setTaskStatusByName(String userId, String name, Status status);

    Task startTaskById(String userId, String id);

    Task startTaskByIndex(String userId, Integer index);

    Task startTaskByName(String userId, String name);

    Task updateTaskById(String userId, String id, String name, String description);

    Task updateTaskByIndex(String userId, Integer index, String name, String description);

    Task updateTaskByName(String userId, String name, String nameNew, String description);

}
