package ru.vmaksimenkov.tm.command.project;

import java.util.concurrent.atomic.AtomicInteger;

import static java.lang.System.out;

public final class ProjectListCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show project list";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        out.println("[PROJECT LIST]");
        out.printf("\t| %-36s | %-12s | %-20s | %-30s | %-30s | %-30s %n", "ID", "STATUS", "NAME", "CREATED", "STARTED", "FINISHED");
        AtomicInteger index = new AtomicInteger(1);
        serviceLocator.getProjectService().findAll(userId).forEach((x) -> out.println(index.getAndIncrement() + "\t" + x));
    }

    @Override
    public String name() {
        return "project-list";
    }

}
