package ru.vmaksimenkov.tm.command.project;

import ru.vmaksimenkov.tm.util.TerminalUtil;

public final class ProjectByIdRemoveCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove project by id";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER ID:");
        serviceLocator.getProjectTaskService().removeProjectById(userId, TerminalUtil.nextLine());
    }

    @Override
    public String name() {
        return "project-remove-by-id";
    }

}
