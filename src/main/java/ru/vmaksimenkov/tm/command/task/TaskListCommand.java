package ru.vmaksimenkov.tm.command.task;

import java.util.concurrent.atomic.AtomicInteger;

import static java.lang.System.out;

public final class TaskListCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show task list";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        out.println("[TASK LIST]");
        out.printf("\t| %-36s | %-12s | %-20s | %-30s | %-30s | %-30s | %s %n", "ID", "STATUS", "NAME", "CREATED", "STARTED", "FINISHED", "PROJECT");
        AtomicInteger index = new AtomicInteger(1);
        serviceLocator.getTaskService().findAll(userId).forEach((x) -> out.println(index.getAndIncrement() + "\t" + x));
    }

    @Override
    public String name() {
        return "task-list";
    }

}
