package ru.vmaksimenkov.tm.repository;

import ru.vmaksimenkov.tm.api.repository.IUserRepository;
import ru.vmaksimenkov.tm.model.User;
import ru.vmaksimenkov.tm.util.HashUtil;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public boolean existsByEmail(final String email) {
        return list.stream()
                .anyMatch(e -> email.equals(e.getEmail()));
    }

    @Override
    public boolean existsByLogin(final String login) {
        return list.stream()
                .anyMatch(e -> login.equals(e.getLogin()));
    }

    @Override
    public boolean existsById(String id) {
        return list.stream()
                .anyMatch(e -> id.equals(e.getId()));
    }

    @Override
    public User findById(final String id) {
        return list.stream()
                .filter(e -> id.equals(e.getId()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public User findByLogin(final String login) {
        return list.stream()
                .filter(e -> login.equals(e.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public void removeByLogin(final String login) {
        remove(list.stream()
                .filter(e -> login.equals(e.getLogin()))
                .findFirst()
                .orElse(null)
        );
    }

    @Override
    public void setPasswordById(final String id, final String password) {
        findById(id).setPasswordHash(HashUtil.salt(password));
    }

}
